# python-wsgi-hello-world
A sample code of Python with WSGI (Web Server Gateway Interface)

### Demo steps:
```sh
oc start-build python-hello
oc scale deploymentconfigs/python-hello --replicas=3
```
